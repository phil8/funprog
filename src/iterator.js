import Debug from 'debug'
let debug = Debug('iterator')

async function * makeAsyncRangeIterator (start = 0, end = Infinity, step = 1, delay = 10) {
    let iterationCount = 0
    for (let i = start; i < end; i += step) {
        iterationCount++
        yield new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(i)
            }, delay)
        })
    }
    return iterationCount
  }

function makeAsyncHasNextRangeIterator (start = 0, end = Infinity, step = 1) {
    let iterationCount = start
    return {
        hasNext: function () {
            return iterationCount < end
        },
        next: function () {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve(iterationCount++)
                }, 10)
            })
        }
    }
}

function makeArrayIterator (dataArray) {
    let iterationCount = 0
    return {
        hasNext: function () {
            return iterationCount < dataArray.length
        },
        next: function () {
            return dataArray[iterationCount++]
            }
        }
    }

function * makeAsyncArrayGenerator (dataArray = [], {
    delay = 10,
    callback = null
}) {
    debug(`Making Async Array Generator from array of length ${dataArray.length}`)
    let iterationCount = 0
    for (let i = 0; i < dataArray.length; i++) {
        iterationCount++
        yield new Promise((resolve, reject) => {
            setTimeout(() => {
                let next = dataArray[i]
                if (callback) { callback(next) }
                debug(next)
                resolve(next)
            }, delay)
        })
    }
}

export {
  makeArrayIterator,
  makeAsyncRangeIterator,
  makeAsyncHasNextRangeIterator,
  makeAsyncArrayGenerator
}
